import HomePage from './pages/HomePage.vue';
import Error from './pages/404Page.vue';
import Login from './pages/LoginPage.vue';
import Registration from './pages/RegistrationPage.vue';

const routes = [
    {path: '/', component: HomePage},
    {path: '/404', component: Error},
    {path: '/registration', component: Registration},
    {path: '/login', component: Login},
];

export default routes;